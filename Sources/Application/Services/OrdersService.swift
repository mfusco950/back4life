//
//  OrdersService.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation

protocol OrdersServiceProtocol {
    //func postOrder(pizzaOrders: [completeOrder], completion: @escaping () -> Void)
  func getOrders(completion: @escaping ([Order]) -> Void)
  func postOrder(order: Order, completion: @escaping () -> Void)
}

class OrdersService: OrdersServiceProtocol {
  
  var ordersRepository: OrdersRepositoryProtocol
  init(ordersRepository: OrdersRepositoryProtocol){
    self.ordersRepository = ordersRepository
  }
  
  func getOrders(completion: @escaping ([Order]) -> Void) {
    ordersRepository.getOrders(completion: completion)
  }
  
  func postOrder(order: Order, completion: @escaping () -> Void) {
    ordersRepository.postOrder(order: order, completion: {
      completion()
    })
  }
    
}
