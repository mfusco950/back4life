//
//  PizzasService.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation

protocol PizzasServiceProtocol {
    func getPizzas(completion: @escaping ([Pizza]) -> Void)
    func postPizza(pizza: Pizza, completion: @escaping () -> Void)
    func patchPizza(pizzaName: String, patchPizza: PizzaPatch, completion: @escaping () -> Void)
}

class PizzasService: PizzasServiceProtocol {
    var pizzasRepository: PizzasRepositoryProtocol
    init(pizzasRepository: PizzasRepositoryProtocol) {
        self.pizzasRepository = pizzasRepository
        
    }
    
    func getPizzas(completion: @escaping ([Pizza]) -> Void) {
        pizzasRepository.getPizzas(completion: completion)
    
    }
    
    func postPizza(pizza: Pizza, completion: @escaping () -> Void) {
        pizzasRepository.postPizza(pizza: pizza, completion: {
            completion()
        })
    }
    
    func patchPizza(pizzaName: String, patchPizza: PizzaPatch, completion: @escaping () -> Void) {
        pizzasRepository.patchPizza(pizzaName: pizzaName, pizza: patchPizza, completion: {
            completion()
        })
    }
}
