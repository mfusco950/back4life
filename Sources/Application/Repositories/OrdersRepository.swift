//
//  OrdersRepository.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation
import SwiftKuery
import SwiftKueryPostgreSQL

protocol OrdersRepositoryProtocol {
  //func postOrder(pizzaOrders: [PizzaOrder], completion: @escaping () -> Void)
  func getOrders(completion: @escaping([Order]) -> Void)
  func postOrder(order: Order, completion: @escaping () -> Void)
}

class OrdersRepository: OrdersRepositoryProtocol {
  
  var dbContext: DatabaseContextProtocol
  init(dbContext: DatabaseContextProtocol) {
    self.dbContext = dbContext
  }
  
  
  func getOrders(completion: @escaping ([Order]) -> Void) {
    
    var completeOrders: [CompleteOrder] = []
    var orders: [Int32: Order] = [:]
    var ordersToSend: [Order] = []
    let completeOrdersTable = CompleteOrderTable()
    
    dbContext.getConnnection(completion: {
      
      connection, error in
      guard let connection = connection else {
        return
      }
      let select = Select(from: completeOrdersTable)
      connection.execute(query: select, onCompletion: {
        result in
        result.asRows(onCompletion: {
          rows, error in
          for row in rows! {
            let order = CompleteOrder(row: row)
            completeOrders.append(order)
            
            if orders[order.id!] != nil {
              
              orders[order.id!]?.pizzaOrders!.append(PizzaOrder(id: order.idPizza!, pizza: order.nomePizza, tipo: order.tipoPizza, prezzo: order.prezzoPizza, quantita: order.quantita, idorder: nil))
              
              
            } else {
              
              var newOrder = Order(id: order.id!, data: order.data, prezzo_totale: order.prezzoTotale, customer: order.customer, pizzaOrders: [])
              let newPizzaOrder = PizzaOrder(id: order.idPizza!, pizza: order.nomePizza, tipo: order.tipoPizza, prezzo: order.prezzoPizza, quantita: order.quantita, idorder: nil)
              newOrder.pizzaOrders = [newPizzaOrder]
              orders[order.id!] = newOrder
              
            }
            
          }
          
          for key in orders.keys {
            
            ordersToSend.append(orders[key]!)
            
          }
          
          completion(ordersToSend)
        })
      })
      
      
    })
    
  }
  
  func postOrder(order: Order, completion: @escaping () -> Void) {
    dbContext.getConnnection(completion: {
      connection, error in
      guard let connection = connection else {
        return
      }
      let orderTable = OrderTable()
      var valueTuples: [(Column, Any)] = []
      for (column, value) in order.toRow() {
        
        if column != "pizzaOrders" {
          
          if column == "data" {
            
            valueTuples.append((Column(column), Date()))
            
          } else {
            if let value = value {
              
              print("\(column) and \(value)")
              valueTuples.append((Column(column), value))
              
            }
            
          }
          
        }
        
      }
      
      print(valueTuples)
      
      var insert = Insert(into: orderTable, valueTuples: valueTuples)
      print(insert.query)
      connection.execute(query: insert, onCompletion: {
        result in
        
        print(result)
        result.asRows(onCompletion: {
          
          rows, error in
          
          print(rows)
          
          for row in rows! {
            
            let newOrder = Order(row: row)
            
            for var pizzaOrder in order.pizzaOrders! {
              
              pizzaOrder.id = newOrder.id!
              
              let pizzaOrderTable = PizzaOrderTable()
              valueTuples = []
              for (column, value) in pizzaOrder.toRow() {
                
                  valueTuples.append((Column(column), value!))
                
              }
              
              insert = Insert(into: pizzaOrderTable, values: valueTuples)
              connection.execute(query: insert, onCompletion: {
                result in
                completion()
              })
              
              
            }
            
          }
          
        })
        completion()
        //                TODO
      })
    })
  }
  
}
