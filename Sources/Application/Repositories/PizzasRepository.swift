//
//  PizzasRepository.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation
import SwiftKuery
import SwiftKueryPostgreSQL
protocol PizzasRepositoryProtocol {
    func getPizzas(completion: @escaping ([Pizza]) -> Void)
    func postPizza(pizza: Pizza, completion: @escaping () -> Void)
    func patchPizza(pizzaName: String, pizza: PizzaPatch, completion: @escaping () -> Void)
}

class PizzasRepository: PizzasRepositoryProtocol {
    var dbContext: DatabaseContextProtocol
    init(dbContext: DatabaseContextProtocol) {
        self.dbContext = dbContext
    }
    
    func getPizzas(completion: @escaping ([Pizza]) -> Void) {
        var pizzas: [Pizza] = []
        let pizzaTable = PizzaTable()
        dbContext.getConnnection(completion: {
            connection, error in
            guard let connection = connection else {
                return
            }
            let select = Select(from: pizzaTable)
            connection.execute(query: select, onCompletion: {
                result in
                result.asRows(onCompletion: {
                    rows, error in
                    for row in rows! {
                        let pizza = Pizza(row: row)
                        pizzas.append(pizza)
                    }
                    completion(pizzas)
                })
            })
        })
    }
    
    func postPizza(pizza: Pizza, completion: @escaping () -> Void) {
        dbContext.getConnnection(completion: {
            connection, error in
            guard let connection = connection else {
                return
            }
            let pizzaTable = PizzaTable()
            var valueTuples: [(Column, Any)] = []
            for (column, value) in pizza.toRow() {
                valueTuples.append((Column(column), value!))
            }
            let insert = Insert(into: pizzaTable, valueTuples: valueTuples)
            connection.execute(query: insert, onCompletion: {
                result in
                completion()
//                TODO
            })
        })
    }
    
    func patchPizza(pizzaName: String, pizza: PizzaPatch, completion: @escaping () -> Void) {
        dbContext.getConnnection(completion: {
            connection, error in
            guard let connection = connection else {
                return
            }
            let pizzaTable = PizzaTable()
            let rows = pizza.toRow()
            
            var set: [(Column, Any)] = []
            
            pizzaTable.columns.forEach({
                column in
                let row = rows[column.name]!!
                switch row {
                case Optional<Any>.none:
                    
                    break
                default:
                    set.append((column, row))
                }
                
            })
            
            let update = Update(pizzaTable, set: set, where: pizzaTable.name == pizzaName)
            connection.execute(query: update, onCompletion: {
                result in
                completion()
                
            })
        })
    }
}
