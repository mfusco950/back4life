//
//  DatabaseContext.swift
//  Application
//
//  Created by Mattia Fusco on 11/03/2019.
//

import Foundation
import SwiftKuery
import SwiftKueryPostgreSQL


protocol DatabaseContextProtocol {
    func getConnnection(completion: @escaping (ConnectionPoolConnection?, Error?) -> Void)
}

class DatabaseContext: DatabaseContextProtocol {
    var pool: ConnectionPool?
    init(){
        connectToDatabase()
    }
    
    func connectToDatabase(){
        
      self.pool = PostgreSQLConnection.createPool(host: "mydbinstance.cw8dlluaze8e.us-east-1.rds.amazonaws.com", port: 5432, options: [.databaseName("pizzeria"), .userName("master"), .password("mypassword")], poolOptions: ConnectionPoolOptions(initialCapacity: 10))

        
    }
    
    func getConnnection(completion: @escaping (ConnectionPoolConnection?, Error?) -> Void)  {
        
        guard let pool = pool else {
        
            return
        }
        pool.getConnection() {
            connection, error in
    
            guard let connection = connection else {
                completion(nil, error)
                return
            }
            completion(connection, nil)
        }
    }
}
