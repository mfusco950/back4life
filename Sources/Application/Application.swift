import Foundation
import Kitura
import LoggerAPI
import Configuration
import CloudEnvironment
import KituraContracts
import Health

public let projectPath = ConfigurationManager.BasePath.project.path
public let health = Health()

public class App {
    let router = Router()
    let cloudEnv = CloudEnv()

    public init() throws {
        // Run the metrics initializer
        initializeMetrics(router: router)
    }

    func postInit() throws {
        // Endpoints
        initializeHealthRoutes(app: self)
        let dbContext = DatabaseContext()
        let pizzasRepository = PizzasRepository(dbContext: dbContext)
        let pizzasService = PizzasService(pizzasRepository: pizzasRepository)
        _ = PizzasController(router: router, pizzasService: pizzasService)
        
      let ordersRepository = OrdersRepository(dbContext: dbContext)
        let ordersService = OrdersService(ordersRepository: ordersRepository)
        _ = OrdersController(router: router, ordersService: ordersService)
        
    }

    public func run() throws {
        try postInit()
        Kitura.addHTTPServer(onPort: cloudEnv.port, with: router)
        Kitura.run()
    }
}
