//
//  Pizza.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation
import SwiftKuery

struct Pizza: ModelProtocol {
    let name: String
    let available: Bool
    let fetta: Double
    let ruoto: Double
    let placchetta: Double
    let teglia: Double
    init(name: String, available: Bool, ruoto: Double, placchetta: Double, fetta: Double, teglia: Double){
        self.name = name
        self.available = available
        self.fetta = fetta
        self.ruoto = ruoto
        self.placchetta = placchetta
        self.teglia = teglia
    }
    
    init(row: [String: Any?]) {
        self.name = row["name"] as! String
        self.available = row["available"] as! Bool
        self.fetta = row["fetta"] as! Double
        self.ruoto = row["ruoto"] as! Double
        self.placchetta = row["placchetta"] as! Double
        self.teglia = row["teglia"] as! Double
    }
}

struct PizzaPatch: ModelProtocol {
    var name: String?
    var available: Bool?
    var fetta: Double?
    var ruoto: Double?
    var placchetta: Double?
    var teglia: Double?
    
    init(name: String, available: Bool, ruoto: Double, placchetta: Double, fetta: Double, teglia: Double){
        self.name = name
        self.available = available
        self.fetta = fetta
        self.ruoto = ruoto
        self.placchetta = placchetta
        self.teglia = teglia
    }
    
}

class PizzaTable: Table {
    let tableName = "pizza"
    let name = Column("name", String.self)
    let available = Column("available", Bool.self)
    let fetta = Column("fetta", Double.self)
    let ruoto = Column("ruoto", Double.self)
    let placchetta = Column("placchetta", Double.self)
    let teglia = Column("teglia", Double.self)
    
}

protocol ModelProtocol: Codable {
    func toRow() -> [String: Any?]
}
    
extension ModelProtocol {
    func toRow() -> [String: Any?] {
        var row: [String: Any?] = [:]
        let mirror = Mirror(reflecting: self)
        for (_, attr) in mirror.children.enumerated() {
            row[attr.label!] = attr.value
        }
        return row
    }
}
