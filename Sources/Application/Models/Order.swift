//
//  Order.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation
import SwiftKuery

struct Order: ModelProtocol {
  let id: Int32?
  let data: Date?
  let prezzo_totale: Double?
  let customer: String?
  var pizzaOrders: [PizzaOrder]?
  
  init(id: Int32, data: Date?, prezzo_totale: Double?, customer: String?, pizzaOrders: [PizzaOrder]) {
    
    self.id = id
    self.data = data
    self.prezzo_totale = prezzo_totale
    self.customer = customer
    self.pizzaOrders = pizzaOrders
    
  }
  
  init(row: [String: Any?]) {
    
    self.id = row["id"] as? Int32
    self.data = row["data"] as? Date
    self.prezzo_totale = row["prezzo_totale"] as? Double
    self.customer = row["customer"] as? String
    
  }
  
}

struct PizzaOrder: ModelProtocol {
  var id: Int32?
  let pizza: String?
  let tipo: String?
  let prezzo: Double?
  let quantita: Int32?
  let idorder: Int32?
  
  init(id: Int32?, pizza: String?, tipo: String?, prezzo: Double?, quantita: Int32?, idorder: Int32?) {
    
    self.id = id
    self.pizza = pizza
    self.tipo = tipo
    self.prezzo = prezzo
    self.quantita = quantita
    self.idorder = idorder
    
  }
  
  init(row: [String: Any?]) {
    
    self.id = row["id"] as? Int32
    self.pizza = row["pizza"] as? String
    self.tipo = row["tipo"] as? String
    self.prezzo = row["prezzo"] as? Double
    self.quantita = row["quantita"] as? Int32
    self.idorder = row["idorder"] as? Int32
    
  }
  
}

struct CompleteOrder: ModelProtocol {
  
  let id: Int32?
  let data: Date?
  let prezzoTotale: Double?
  let customer: String?
  let idPizza: Int32?
  let nomePizza: String?
  let tipoPizza: String?
  let prezzoPizza: Double?
  let quantita: Int32?
  
  init(id: Int32, data: Date?, prezzoTotale: Double?, customer: String?, idPizza: Int32?, nomePizza: String?, tipoPizza: String?, prezzoPizza: Double?, quantita: Int32?) {
    
    self.id = id
    self.data = data
    self.prezzoTotale = prezzoTotale
    self.customer = customer
    self.idPizza = idPizza
    self.nomePizza = nomePizza
    self.tipoPizza = tipoPizza
    self.prezzoPizza = prezzoPizza
    self.quantita = quantita
    
  }
  
  init(row: [String: Any?]) {
    
    self.id = row["id"] as? Int32
    self.data = row["data"] as? Date
    self.prezzoTotale = row["prezzo_totale"] as? Double
    self.customer = row["customer"] as? String
    self.idPizza = row["id_pizza"] as? Int32
    self.nomePizza = row["pizza"] as? String
    self.tipoPizza = row["tipo"] as? String
    self.prezzoPizza = row["prezzo"] as? Double
    self.quantita = row["quantita"] as? Int32
    
  }
  
}

class OrderTable: Table {
  
  let tableName = "order"
  let id = Column("id", Int32.self)
  let data = Column("date")
  let prezzo_totale = Column("prezzo_totale", Double.self)
  let customer = Column("customer", String.self)
  
}

class PizzaOrderTable: Table {
  
  let tableName = "pizzaorder"
  let id = Column("id", Int32.self)
  let idorder = Column("idorder", Int32.self)
  let pizza = Column("pizza", String.self)
  let tipo = Column("tipo", String.self)
  let prezzo = Column("prezzo", Double.self)
  let quantita = Column("quantita", Int32.self)
  
}

class CompleteOrderTable: Table {
  
  let tableName = "complete_order"
  let id = Column("id", Int32.self)
  let data = Column("date")
  let prezzo = Column("prezzo_totale", Double.self)
  let customer = Column("customer", String.self)
  let idPizza = Column("id_pizza", Int32.self)
  let nomePizza = Column("pizza", String.self)
  let tipoPizza = Column("tipo", String.self)
  let prezzoPizza = Column("prezzo", Double.self)
  let quantitaPizza = Column("quantita", Int32.self)
  
}


