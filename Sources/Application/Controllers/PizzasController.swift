//
//  PizzasController.swift
//  Application
//
//  Created by Mattia Fusco on 12/03/2019.
//

import Foundation
import Kitura
import KituraContracts


struct PizzaPatchParams: QueryParams {
    let pizzaName: String
}


struct OptionalUser: Codable {
    
}

class PizzasController {
    var pizzasService: PizzasServiceProtocol?
    init(router: Router, pizzasService: PizzasServiceProtocol){
        self.pizzasService = pizzasService
        initRoutes(router: router)
    }
    
    func initRoutes(router: Router) {
        router.get("/pizzas", handler: getPizzas)
        router.post("/pizzas", handler: postPizza)
        router.patch("/pizzas", handler: patchPizza)
        
    }
    
    func getPizzas(completion: @escaping ([Pizza]?, RequestError?) -> Void){
        pizzasService?.getPizzas(completion: {
            pizze in
            completion(pizze, nil)
        })
        

    }
    
    func postPizza(item: Pizza?, completion: @escaping (Pizza?, RequestError?) -> Void) {
        pizzasService?.postPizza(pizza: item!, completion: {
            completion(nil, nil)
        })
        
    }
    
    func patchPizza(pizzaName: String, patchPizza: PizzaPatch, completion: @escaping (Pizza?, RequestError?) -> Void) {
        
        pizzasService?.patchPizza(pizzaName: pizzaName, patchPizza: patchPizza, completion: {
            completion(nil, nil)
        })
        
        
    }
}
