//
//  OrdersController.swift
//  Application
//
//  Created by Mattia Fusco on 11/03/2019.
//

import Foundation
import Kitura
import KituraContracts

class DeleteQuery: QueryParams {
    var id: Int
}

class OrdersController {
    var ordersService: OrdersServiceProtocol
    init(router: Router, ordersService: OrdersServiceProtocol){
        self.ordersService = ordersService
        initRoutes(router: router)
    }
    
    func initRoutes(router: Router){
        router.get("/orders", handler: getOrders)
        router.post("/orders", handler: postOrder)
        router.delete("/orders", handler: deleteOrder)
    }
    
  func getOrders(completion: @escaping ([Order]?, RequestError?) -> Void) {
        ordersService.getOrders(completion: {
          
          orders in
          completion(orders, nil)
          
        })
    }
  
  func postOrder(item: Order?, completion: @escaping (Order?, RequestError?) -> Void) {
    ordersService.postOrder(order: item!, completion: {
      completion(nil, nil)
    })
    
  }
  
    
    func deleteOrder(params: DeleteQuery, handler: @escaping ResultClosure) {
        print(params.id)
        handler(nil)
    }
}
